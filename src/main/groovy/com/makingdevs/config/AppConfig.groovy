package com.makingdevs.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@Configuration
class AppConfig extends WebMvcConfigurerAdapter {

    @Bean
    ConfigObject config() {
        def userHome = System.properties["user.home"]

        def environment = System.getenv("ENVIRONMENT") ?: 'development'

        def file = new File("${userHome}/.todomvc/configuration-${environment}.groovy")
        if(!file.exists())
            throw new RuntimeException("""
        The file ${userHome}/.todomvc/configuration-${environment}.groovy doesn't exists,
        create the directory, and the file, and copy the config, and adjust it
        so can you check the wiki page?
        https://github.com/makingdevs/emailer-app/blob/master/readme.md
        """)

        new ConfigSlurper().parse(file.text)
    }

}