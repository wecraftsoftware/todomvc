package com.makingdevs.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.view.InternalResourceViewResolver
import org.springframework.web.servlet.view.JstlView

/**
 * Created by makingdevs on 6/15/17.
 */
@EnableWebMvc
@Configuration
@ComponentScan("com.makingdevs.web")
class WebConfig {

  @Bean
  ViewResolver viewResolver() {
    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver()
    viewResolver.setViewClass(JstlView.class)
    viewResolver.setPrefix("/WEB-INF/views/")
    viewResolver.setSuffix(".jsp")

    return viewResolver
  }

}
