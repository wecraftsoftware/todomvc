package com.makingdevs.config

import groovy.transform.CompileStatic
import liquibase.integration.spring.SpringLiquibase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Configurable
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.JpaVendorAdapter
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.Database
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager

import javax.sql.DataSource
import java.lang.management.PlatformManagedObject

/**
 * Created by makingdevs on 6/15/17.
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.makingdevs.repositories")
@ComponentScan(basePackages = "com.makingdevs.service")
class DataAccessConfig {

  @Autowired
  ConfigObject configuration

  @Bean
  DataSource dataSource(){
    DriverManagerDataSource ds = new DriverManagerDataSource()
    ds.driverClassName = configuration.database.driver
    ds.username = configuration.database.username
    ds.password = configuration.database.password
    ds.url = configuration.database.url
    ds
  }

  @Bean
  LocalContainerEntityManagerFactoryBean entityManagerFactory(){
    LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean()
    lef.dataSource = dataSource()
    lef.jpaVendorAdapter = jpaVendorAdapter()
    lef.packagesToScan = ["com.makingdevs.model"]
    lef
  }

  @Bean
  JpaVendorAdapter jpaVendorAdapter(){
    HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter()
    adapter.with {
      showSql = true
      generateDdl = false
      database = Database.MYSQL
    }
    adapter
  }

  @Bean
  PlatformTransactionManager transactionManager(){
    new JpaTransactionManager()
  }

  @Bean
  SpringLiquibase liquibase(){
    new SpringLiquibase(
        dataSource: dataSource(),
        changeLog: "classpath:db-changelog.xml")
  }
}
