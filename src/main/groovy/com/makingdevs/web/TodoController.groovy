package com.makingdevs.web

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class TodoController {


    @RequestMapping("/test")
    public String test() {
        return "Hello World";
    }
}