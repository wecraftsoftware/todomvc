package com.makingdevs.web

import com.makingdevs.config.AppConfig
import com.makingdevs.config.DataAccessConfig
import com.makingdevs.config.WebConfig
import groovy.transform.CompileStatic
import org.springframework.web.WebApplicationInitializer
import org.springframework.web.context.ContextLoaderListener
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import org.springframework.web.servlet.DispatcherServlet

import javax.servlet.ServletContext
import javax.servlet.ServletException
import javax.servlet.ServletRegistration

/**
 * Created by makingdevs on 6/15/17.
 */
@CompileStatic
class WebAppInitializer implements WebApplicationInitializer {
  @Override
  void onStartup(ServletContext container) throws ServletException {

    // Create the 'root' Spring application context
    AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext()
    rootContext.register(AppConfig, DataAccessConfig)
    // Manage the lifecycle of the root application context
    container.addListener(new ContextLoaderListener(rootContext))
    // Create the dispatcher servlet's Spring application context
    AnnotationConfigWebApplicationContext dispatcherServlet = new AnnotationConfigWebApplicationContext()
    dispatcherServlet.register(WebConfig)
    // Register and map the dispatcher servlet
    ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(dispatcherServlet))
    dispatcher.setLoadOnStartup(1)
    dispatcher.addMapping("/")

  }
}
