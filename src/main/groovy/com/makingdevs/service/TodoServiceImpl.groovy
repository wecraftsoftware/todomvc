package com.makingdevs.service

import com.makingdevs.model.Todo
import com.makingdevs.repositories.TodoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TodoServiceImpl implements TodoService {

    @Autowired
    TodoRepository todoRepository

    Long add(Todo todo) {
        return todoRepository.saveAndFlush(todo)
    }
    
    List<Todo> list() {
        todoRepository.findAll()
    }
}