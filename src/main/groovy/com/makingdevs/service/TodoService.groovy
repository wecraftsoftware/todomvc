package com.makingdevs.service

import com.makingdevs.model.Todo

interface TodoService {

    Long add(Todo todo)

    List<Todo> list()

}