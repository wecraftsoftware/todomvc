package com.makingdevs.model
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.GenerationType

@Entity
class Todo {

	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	long id
	String name
	String description
	boolean active

}
