--liquibase formatted sql

--changeset neodevelop:2
CREATE TABLE todo (
	id BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(200) NOT NULL,
	description VARCHAR(300) NOT NULL,
	active  tinyint(1) 
)

--changeset neodevelop:3
insert into todo(name, description, active) values("Todo 1", "Description 1", false);
insert into todo(name, description, active) values("Todo 2", "Description 2", true);
