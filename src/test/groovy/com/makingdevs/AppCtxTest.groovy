package com.makingdevs

import com.makingdevs.config.AppConfig
import com.makingdevs.config.DataAccessConfig
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

/**
 * Created by makingdevs on 6/15/17.
 */
@RunWith(SpringJUnit4ClassRunner)
@ContextConfiguration(classes = [AppConfig, DataAccessConfig])
class AppCtxTest {

  @Autowired
  ApplicationContext context

  @Test
  void testAppCtx(){
    Assert.assertNotNull(context)
  }
}
