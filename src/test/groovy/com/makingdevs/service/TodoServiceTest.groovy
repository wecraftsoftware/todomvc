import com.makingdevs.service.TodoService
import com.makingdevs.service.TodoServiceImpl
import spock.lang.Specification
import spock.lang.Unroll

class TodoServiceTest extends Specification {

    TodoService todoService = new TodoServiceImpl()

    @Unroll("Add a new Todo")
    def "Add a new Todo"() {
        when:
        1
        then:
        true
    }
}